//
//  ConsumptionTableViewCell.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/16.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

class ConsumptionTableViewCell: UITableViewCell {

    @IBOutlet weak var MemberLabel: UILabel!
    @IBOutlet weak var PhoneLabel: UILabel!
    @IBOutlet weak var MoneyLabel: UILabel!
    @IBOutlet weak var StoreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
