//
//  MemberTableViewCell.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/18.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

class MemberTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var DateTimeLabel: UILabel!
    
    @IBOutlet weak var PhoneLabel: UILabel!
    
    @IBOutlet weak var MemberLabel: UILabel!
    
    @IBOutlet weak var MoneyLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
