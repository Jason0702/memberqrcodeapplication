//
//  NetWorkJob.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/13.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

var Store_Number: Int!
var Token: String!
var Member: [MemberItem] = []
var Shopping: [ShoppingItem] = []
var Stored: [StoredItem] = []
var EmployeeUser = [String:String]()

public class NetWorkJob{
    
    let TotalUrl = "http://dev.twgo.asia/member-qrcode"
    
    let LoginUrl = "/api/v1/login"
    //GET
    let ShopRecordURL = "/api/v1/vip-shopping-record"
    let StoreRecordURL = "/api/v1/vip-stored-record"
    
    //POST
    let ShoppingRecordURL = "/api/v1/vip-shopping-record"
    let StoredRecordURL = "/api/v1/vip-stored-record"
    
    let MembersUrl = "/api/v1/vips"
    
    
    
    
    //登入
    func ShopLogin(Account: String, Password: String, isSave: Bool){
        let param: Parameters = ["name": Account,
                                 "password": Password]
        Alamofire.request(TotalUrl+LoginUrl, method: .post, parameters: param, encoding: JSONEncoding.default).validate().responseJSON{
            responce in
            
            switch responce.result {
            case .success:
                print("Validation Successful")
                if let json = responce.data{
                    do{
                        let datas = try JSON(data: json)
                        let code = datas["code"].stringValue
                        let status = datas["status"].stringValue
                        let data = datas["data"]
                        let id = data["id"].intValue
                        let token = data["token"].stringValue
                        let Realname = data["real_name"].stringValue
                        print("RealName \(Realname)")
                        print("Code \(code)")
                        print("Status \(status)")
                        print("ID \(id)")
                        print("Token \(token)")
                        
                        RealName = Realname
                        Token = token
                        Store_Number = id
                        print("isSave\(isSave)")
                        self.SaveUser(Account: Account, Password: Password, isSave: isSave)
                        
                        
                        if !token.isEmpty{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller =
                                storyboard.instantiateViewController(withIdentifier: "HomeViewController")
                            UIApplication.topViewController()!.present(controller, animated: true, completion: nil)
                        }
                        
                    }catch let error{
                        print("Error \(error)")
                    }
                }
                break
            case .failure(let error):
                print("Error Fail\(error)")
                self.AlertController(sendText: "請檢查帳號或密碼是否輸入錯誤！")
            }
        }
    }
    
    //取得全部消費記錄
    func GetShoppingRecord(){
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        Alamofire.request(TotalUrl+ShopRecordURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
            responce in
    
            if let headers = responce.response?.allHeaderFields as? [String: String]{
                if headers.keys.contains("Authorization"){
                    if !headers["Authorization"]!.isEmpty {
                        Token = headers["Authorization"]
                        print(Token!)
                    }
                }
            }
            switch responce.result{
                case .success:
                    print("Vlidation Successful")
                    if let json = responce.data{
                        do{
                            let datas = try JSON(data: json)
                            let code = datas["code"]
                            if let data = datas["data"].array{
                                for mdata in data{
                                    let Id = mdata["id"].intValue
                                    let Store_number = mdata["store_number"].stringValue
                                    let Store_name = mdata["store_name"].stringValue
                                    let Serial_number = mdata["serial_number"].stringValue
                                    let Billing_date = mdata["billing_date"].stringValue
                                    let Vip_number = mdata["vip_number"].stringValue
                                    let Vip_name = mdata["vip_name"].stringValue
                                    let Depostit_pay = mdata["deposit_pay_money"].stringValue
                                    let Total_price = mdata["total_price"].stringValue
                                    let Is_valid = mdata["is_valid"].stringValue
                                    let Created_at = mdata["created_at"].stringValue
                                    let Updated_at = mdata["updated_at"].stringValue
                                    Shopping.append(ShoppingItem(id: Id, store_number: Store_number, store_name: Store_name, serial_number: Serial_number, billing_date: Billing_date, number: Vip_number, name: Vip_name, depostit_pay: Depostit_pay, total_price: Total_price, valid: Is_valid, created_at: Created_at, update_at: Updated_at))
                                    print("ID: \(Vip_name) Money\(Total_price)")
                                }
                                print("全部消費記錄\(code)")
                                NotificationCenter.default.post(name: .Shopreload, object: nil)
                            }
                        }catch let error{
                            print(error)
                        }
                    }
                    break
                case .failure(let error):
                        print(error)
            }
        }
    }
    
    //取得全部儲值記錄
    func GetStoredRecord(){
         let headers : HTTPHeaders = ["Authorization":Token!,
                                      "Content-Type":"application/json"]
        Alamofire.request(TotalUrl+StoreRecordURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                print("Code \(code)")
                                if let data = datas["data"].array{
                                    for mdata in data{
                                        let Id = mdata["id"].intValue
                                        let Vip_id = mdata["vip_id"].stringValue
                                        let Store_number = mdata["store_number"].stringValue
                                        let Store_name = mdata["store_name"].stringValue
                                        let Serial_number = mdata["serial_number"].stringValue
                                        let Vip_number = mdata["vip_number"].stringValue
                                        let Vip_name = mdata["vip_name"].stringValue
                                        let Cash_Money = mdata["cash_money"].stringValue
                                        let Depostit_pay = mdata["deposit_money"].stringValue
                                        let Work_place = mdata["work_place"].stringValue
                                        let Pos_class = mdata["pos_class"].stringValue
                                        let Is_valid = mdata["is_valid"].stringValue
                                        let Remark = mdata["remark"].stringValue
                                        let Created_at = mdata["created_at"].stringValue
                                        let Updated_at = mdata["updated_at"].stringValue
                                        Stored.append(StoredItem(id: Id, vip_id: Vip_id, store_number: Store_number, store_name: Store_name, serial_number: Serial_number, number: Vip_number, name: Vip_name, cash_money: Cash_Money, depostit_pay: Depostit_pay, work_place: Work_place, pos_class: Pos_class, valid: Is_valid, remark: Remark, created_at: Created_at, update_at: Updated_at))
                                        
                                        print("Dep\(Depostit_pay)")
                                        
                                    }
                                    
                                    print("全部儲值記錄\(code)")
                                    
                                    
                                    
                                    NotificationCenter.default.post(name: .Storeload, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
        
    }
    
    //用日期取得消費紀錄
    func DateGetShoppingRecord(StartDate: String, EndDate: String){
        Shopping.removeAll()
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        
        let DateGetShoppingRecordURL = TotalUrl + "/api/v1/start/\(StartDate)/end/\(EndDate)/vip-shopping-record"
        
        Alamofire.request(DateGetShoppingRecordURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                print("Code \(code)")
                                if let data = datas["data"].array{
                                for mdata in data{
                                    let Id = mdata["id"].intValue
                                    let Store_number = mdata["store_number"].stringValue
                                    let Store_name = mdata["store_name"].stringValue
                                    let Serial_number = mdata["serial_number"].stringValue
                                    let Billing_date = mdata["billing_date"].stringValue
                                    let Vip_number = mdata["vip_number"].stringValue
                                    let Vip_name = mdata["vip_name"].stringValue
                                    let Depostit_pay = mdata["deposit_pay_money"].stringValue
                                    let Total_price = mdata["total_price"].stringValue
                                    let Is_valid = mdata["is_valid"].stringValue
                                    let Created_at = mdata["created_at"].stringValue
                                    let Updated_at = mdata["updated_at"].stringValue
                                    Shopping.append(ShoppingItem(id: Id, store_number: Store_number, store_name: Store_name, serial_number: Serial_number, billing_date: Billing_date, number: Vip_number, name: Vip_name, depostit_pay: Depostit_pay, total_price: Total_price, valid: Is_valid, created_at: Created_at, update_at: Updated_at))
                                    print("ID: \(Vip_name) Money\(Total_price)")
                                    }
                                    
                                    print("日期取得消費紀錄\(code)")
                                    
                                    
                                    
                                    NotificationCenter.default.post(name: .Shopreload, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
    }
    
    //用日期取得儲值記錄
    func DateGetStoreRecord(StartDate: String, EndDate: String){
        Stored.removeAll()
        
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        
        let DateGetStoreRecord = TotalUrl + "/api/v1/start/\(StartDate)/end/\(EndDate)/vip-stored-record"
        
        Alamofire.request(DateGetStoreRecord, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                print("Code \(code)")
                                if let data = datas["data"].array{
                                    for mdata in data{
                                        let Id = mdata["id"].intValue
                                        let Vip_id = mdata["vip_id"].stringValue
                                        let Store_number = mdata["store_number"].stringValue
                                        let Store_name = mdata["store_name"].stringValue
                                        let Serial_number = mdata["serial_number"].stringValue
                                        let Vip_number = mdata["vip_number"].stringValue
                                        let Vip_name = mdata["vip_name"].stringValue
                                        let Cash_Money = mdata["cash_money"].stringValue
                                        let Depostit_pay = mdata["deposit_money"].stringValue
                                        let Work_place = mdata["work_place"].stringValue
                                        let Pos_class = mdata["pos_class"].stringValue
                                        let Is_valid = mdata["is_valid"].stringValue
                                        let Remark = mdata["remark"].stringValue
                                        let Created_at = mdata["created_at"].stringValue
                                        let Updated_at = mdata["updated_at"].stringValue
                                        Stored.append(StoredItem(id: Id, vip_id: Vip_id, store_number: Store_number, store_name: Store_name, serial_number: Serial_number, number: Vip_number, name: Vip_name, cash_money: Cash_Money, depostit_pay: Depostit_pay, work_place: Work_place, pos_class: Pos_class, valid: Is_valid, remark: Remark, created_at: Created_at, update_at: Updated_at))
                                        
                                        print("Dep\(Depostit_pay)")
                                        
                                    }
                                    
                                    print("日期取得儲值記錄\(code)")
                                    
                                    NotificationCenter.default.post(name: .Storeload, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
        
    }
    
    //用帳號日期取得消費記錄
    func AccountDateGetShoppingRecord(Account: String, StartDate: String, EndDate: String){
        Shopping.removeAll()
        
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        
        let AccountDateGetShoppingRecordURL = TotalUrl + "/api/v1/start/\(StartDate)/end/\(EndDate)/account/\(Account)/vip-shopping-record"
        
        Alamofire.request(AccountDateGetShoppingRecordURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                print("Code \(code)")
                              if let data = datas["data"].array{
                                print("data\(data)")
                               for mdata in data{
                                   let Id = mdata["id"].intValue
                                   let Store_number = mdata["store_number"].stringValue
                                let Store_name = mdata["store_name"].stringValue
                                   let Serial_number = mdata["serial_number"].stringValue
                                   let Billing_date = mdata["billing_date"].stringValue
                                   let Vip_number = mdata["vip_number"].stringValue
                                   let Vip_name = mdata["vip_name"].stringValue
                                   let Depostit_pay = mdata["deposit_pay_money"].stringValue
                                   let Total_price = mdata["total_price"].stringValue
                                   let Is_valid = mdata["is_valid"].stringValue
                                   let Created_at = mdata["created_at"].stringValue
                                   let Updated_at = mdata["updated_at"].stringValue
                                   Shopping.append(ShoppingItem(id: Id, store_number: Store_number, store_name: Store_name, serial_number: Serial_number, billing_date: Billing_date, number: Vip_number, name: Vip_name, depostit_pay: Depostit_pay, total_price: Total_price, valid: Is_valid, created_at: Created_at, update_at: Updated_at))
                                   print("ID: \(Vip_name) Money\(Total_price)")
                                    
                                }
                                    
                                    print("帳號日期取得消費記錄\(code)")
                                    
                                    
                                    
                                    NotificationCenter.default.post(name: .Shopreload, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
        
    }
    
    //用帳號日期取得儲值記錄
    func AccountDateGetStoreRecord(Account: String, StartDate: String, EndDate: String){
        Stored.removeAll()
        
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        
        let AccountDataGetStoreRecordURL = TotalUrl + "/api/v1/start/\(StartDate)/end/\(EndDate)/account/\(Account)/vip-stored-record"
        
        Alamofire.request(AccountDataGetStoreRecordURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                print("Code \(code)")
                                if let data = datas["data"].array{
                                    print("data\(data)")
                                    for mdata in data{
                                        let Id = mdata["id"].intValue
                                        let Vip_id = mdata["vip_id"].stringValue
                                        let Store_number = mdata["store_number"].stringValue
                                        let Store_name = mdata["store_name"].stringValue
                                        let Serial_number = mdata["serial_number"].stringValue
                                        let Vip_number = mdata["vip_number"].stringValue
                                        let Vip_name = mdata["vip_name"].stringValue
                                        let Cash_Money = mdata["cash_money"].stringValue
                                        let Depostit_pay = mdata["deposit_money"].stringValue
                                        let Work_place = mdata["work_place"].stringValue
                                        let Pos_class = mdata["pos_class"].stringValue
                                        let Is_valid = mdata["is_valid"].stringValue
                                        let Remark = mdata["remark"].stringValue
                                        let Created_at = mdata["created_at"].stringValue
                                        let Updated_at = mdata["updated_at"].stringValue
                                        Stored.append(StoredItem(id: Id, vip_id: Vip_id, store_number: Store_number, store_name: Store_name, serial_number: Serial_number, number: Vip_number, name: Vip_name, cash_money: Cash_Money, depostit_pay: Depostit_pay, work_place: Work_place, pos_class: Pos_class, valid: Is_valid, remark: Remark, created_at: Created_at, update_at: Updated_at))
                                        
                                        print("Dep\(Depostit_pay)")
                                        
                                    }
                                    
                                    print("帳號日期取得儲值記錄\(code)")
                                    
                                    
                                    
                                    NotificationCenter.default.post(name: .Storeload, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
    }
    
    //上傳扣款記錄
    func POSTShoppingRecord(UserID: String, DateTime: String, Price: String, Remark: String, EmployeeID: String, isPrint: Bool){
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        print(Token!)
        
        let param: Parameters = [
            "store_number": Store_Number!,
            "billing_date": DateTime,
            "vip_id": UserID,
            "deposit_pay_money": "0",
            "total_price": Price,
            "remark": Remark,
            "employee_id": EmployeeID
        ]
        print("Param\(param)")
        Alamofire.request(TotalUrl+ShoppingRecordURL, method: .post, parameters: param , encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                let data = datas["data"]
                                
                                let remark = data["remark"].stringValue
                                let vip_account = data["vip_account"].stringValue
                                let vip_name = data["vip_name"].stringValue
                                let employee_id = data["employee_id"].stringValue
                                let created_at = data["created_at"].stringValue
                                let store_name = data["store_name"].stringValue
                                let totalprice = data["total_price"].stringValue
                                let walletbalance = data["wallet_balance"].stringValue
                                let last_wallet_balance = data["last_wallet_balance"].stringValue
                                
                                print("Code \(code)")
                                print("扣款 \(data)")
                                let printobj = Print(Paytype: "扣款", remark: remark, vipaccount: vip_account, vipname: vip_name, createdat: created_at, employeeid: employee_id, totalprice: totalprice, storename: store_name, walletbalance: walletbalance, depositmoney: "", lastwalletbalance: last_wallet_balance)
                                
                                if isPrint{
                                    NotificationCenter.default.post(name: .PrintList, object: printobj)
                                    
                                }else{
                                    NotificationCenter.default.post(name: .NoPrint, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
        
        /*
         扣款 {
           "wallet_balance" : 4200,
           "id" : 36,
           "billing_date" : "2020-03-31",
           "store_name" : "測試商家",
           "employee_id" : "1",
           "is_valid" : null,
           "last_wallet_balance" : 4400,
           "vip_account" : "0900111222",
           "store_number" : 12,
           "updated_at" : "2020-03-31 11:55:01",
           "vip_name" : "Js",
           "created_at" : "2020-03-31 11:55:01",
           "remark" : "無",
           "total_price" : "200",
           "vip_id" : "7",
           "deposit_pay_money" : "0",
           "serial_number" : "A00000036"
         }
         **/
    }
    //上傳儲值紀錄
    func POSTStoredRecord(UserID: String, Price: String, Remark: String, EmployeeID: String, isPrint: Bool){
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        print(Token!)
        
        let param: Parameters = [
             "store_number": Store_Number!,
             "vip_id": UserID,
             "deposit_money": Price,
             "payment": "現金",
             "remark": Remark,
             "employee_id": EmployeeID
        ]
        print("Param \(param)")
        Alamofire.request(TotalUrl + StoredRecordURL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let data = datas["data"]
                                let remark = data["remark"].stringValue
                                let vip_account = data["vip_account"].stringValue
                                let vip_name = data["vip_name"].stringValue
                                let employee_id = data["employee_id"].stringValue
                                let created_at = data["created_at"].stringValue
                                let deposit_money = data["deposit_money"].stringValue
                                let store_name = data["store_name"].stringValue
                                let walletbalance = data["wallet_balance"].stringValue
                                let last_wallet_balance = data["last_wallet_balance"].stringValue
                                
                                print("Name \(vip_name)")
                                print("儲值 \(data)")
                                
                                let printobj = Print(Paytype: "儲值", remark: remark, vipaccount: vip_account, vipname: vip_name, createdat: created_at, employeeid: employee_id, totalprice: "", storename: store_name, walletbalance: walletbalance, depositmoney: deposit_money, lastwalletbalance: last_wallet_balance)
                                
                                if isPrint{
                                    NotificationCenter.default.post(name: .PrintList, object: printobj)
                                }else{
                                    NotificationCenter.default.post(name: .NoPrint, object: nil)
                                }
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
        
        
        /***
         儲值 {
           "store_name" : "測試商家",
           "employee_id" : "1",
           "work_place" : null,
           "id" : 60,
           "pos_class" : null,
           "deleted_at" : "",
           "serial_number" : "A00000060",
           "sales_man" : null,
           "vip_name" : "Js",
           "store_number" : 12,
           "deposit_money" : "100",
           "remark" : "無",
           "payment" : "現金",
           "is_valid" : null,
           "vip_account" : "0900111222",
           "updated_at" : "2020-03-31 11:37:05",
           "vip_id" : "7",
           "cash_money" : null,
           "last_wallet_balance" : 4020,
           "created_at" : "2020-03-31 11:37:05"
         }         **/
    }
    
    
    
    func GetMember(){
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        print(Token!)
        
        
        Alamofire.request(TotalUrl + MembersUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"]
                                  if let data = datas["data"].array{
                                    for mdata in data{
                                        let VipNumber = mdata["vip_number"].stringValue
                                        let VipName = mdata["name"].stringValue
                                        let Account = mdata["account"].stringValue
                                        let Balance = mdata["wallet_balance"].stringValue
                                                              
                                        print("Code \(code)")
                                                          
                                        print("Name \(VipName)")
                                        print("Number \(VipNumber)")
                                        print("Phone \(Account)")
                                        print("Balance \(Balance)")
                                        Member.append(MemberItem(datetime: VipNumber, phone: Account, member: VipName, money: Balance))
                                    }
                                    NotificationCenter.default.post(name: .reload, object: nil)
                                }
                                
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                }
            }
    }
    
    //對話框
    func AlertController(sendText: String){
        let alertController = UIAlertController(title: "提示",
                        message: sendText, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "知道了", style: .default, handler: {
            action in
        })
        alertController.addAction(okAction)
        UIApplication.topViewController()!.present(alertController, animated: true, completion: nil)
    }
    
    func SaveUser(Account: String, Password: String, isSave: Bool){
        print("我存了什麼？\(Account) \(Password) \(isSave)")
        let ud: UserDefaults = UserDefaults.standard
        ud.set(Account, forKey: "Account")
        if isSave {
            ud.set(Password, forKey: "Password")
            ud.set(isSave, forKey: "isSave")
        }else{
            ud.set(isSave, forKey: "isSave")
        }
        
    }
    
    //店員編號確認
    func GetEmployeeCheck(EmployeeID: String){
        
        let headers : HTTPHeaders = ["Authorization":Token!,
                                     "Content-Type":"application/json"]
        print(Token!)
        let EmployeeNumberUrl = TotalUrl + "/api/v1/employees-number/\(EmployeeID)"
        
        Alamofire.request(EmployeeNumberUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers: headers).validate().responseJSON{
                responce in
        
                if let headers = responce.response?.allHeaderFields as? [String: String]{
                    if headers.keys.contains("Authorization"){
                        if !headers["Authorization"]!.isEmpty {
                            Token = headers["Authorization"]
                            print(Token!)
                        }
                    }
                }
                switch responce.result{
                    case .success:
                        print("Vlidation Successful")
                        if let json = responce.data{
                            do{
                                let datas = try JSON(data: json)
                                let code = datas["code"].stringValue
                                if let data = datas["data"].array{
                                    for mdata in data{
                                        let ID = mdata["id"].stringValue
                                        let Name = mdata["name"].stringValue
                                        EmployeeUser = [ID:Name]
                                    }
                                }
                                NotificationCenter.default.post(name: .EmployeeEntry, object: nil)
                            }catch let error{
                                print(error)
                            }
                        }
                        break
                    case .failure(let error):
                            print(error)
                    NotificationCenter.default.post(name: .EmployeeEntry, object: nil)
                }
            }
    }
}


extension Notification.Name {
    static let reload = Notification.Name("reload")
    static let Shopreload = Notification.Name("Shopreload")
    static let Storeload = Notification.Name("Storeload")
    static let EmployeeEntry = Notification.Name("EmployeeEntry")
    static let PrintList = Notification.Name("PrintList")
    static let NoPrint = Notification.Name("NoPrint")
}
