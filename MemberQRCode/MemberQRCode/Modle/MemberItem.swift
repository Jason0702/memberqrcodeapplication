//
//  MemberItem.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/18.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation

class MemberItem{
    var DateTime: String
    var Phone : String
    var Member : String
    var Money : String
    required init(datetime: String, phone: String, member: String, money: String){
        self.DateTime = datetime
        self.Phone = phone
        self.Member = member
        self.Money = money
    }
    var setDateTime: String{
        get{
            return self.DateTime
        }
        set{
            self.DateTime = newValue
        }
    }
    var setPhone: String{
        get{
            return self.Phone
        }
        set{
            self.Phone = newValue
        }
    }
    var setMember: String{
        get{
            return self.Member
        }
        set{
            self.Member = newValue
        }
    }
    var setMoney: String{
        get{
            return self.Money
        }
        set{
            self.Money = newValue
        }
    }
}
