//
//  Print.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/30.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation

class Print{
    let PayType: String
    let Remark: String
    let VipAccount: String
    let VipName: String
    let EmployeeID: String
    let CreatedAt: String
    let DepositMoney: String
    let StoreName: String
    let TotalPrice: String
    let WalletBalance: String
    let LastWalletBalance: String
    
    init(Paytype: String,remark: String, vipaccount: String, vipname: String, createdat: String, employeeid: String, totalprice: String, storename: String, walletbalance: String, depositmoney: String, lastwalletbalance: String){
        self.PayType = Paytype
        self.Remark = remark
        self.VipAccount = vipaccount
        self.VipName = vipname
        self.CreatedAt = createdat
        self.EmployeeID = employeeid
        self.TotalPrice = totalprice
        self.StoreName = storename
        self.WalletBalance = walletbalance
        self.DepositMoney = depositmoney
        self.LastWalletBalance = lastwalletbalance
        print("\(PayType) is initialized")
    }
    deinit {
        print("\(PayType) is deinitialized")
    }
}
