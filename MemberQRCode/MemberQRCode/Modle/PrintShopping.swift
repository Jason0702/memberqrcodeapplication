//
//  PrintShopping.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/30.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation

class PrintShopping{
    
    var Remark: String
    var VipAccount: String
    var VipName: String
    var CreatedAt: String
    var EmployeeID: String
    var TotalPrice: String
    var StoreName: String
    var WalletBalance: String
    
    required init(remark: String, vipaccount: String, vipname: String, createdat: String, employeeid: String, totalprice: String, storename: String, walletbalance: String){
        self.Remark = remark
        self.VipAccount = vipaccount
        self.VipName = vipname
        self.CreatedAt = createdat
        self.EmployeeID = employeeid
        self.TotalPrice = totalprice
        self.StoreName = storename
        self.WalletBalance = walletbalance
    }
}
