//
//  PrintStore.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/30.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation

class PrintStore{
    var Remark: String
    var VipAccount: String
    var Payment: String
    var SerialNumber: String
    var VipName: String
    var EmployeeID: String
    var CreatedAt: String
    var DepositMoney: String
    var StoreName: String
    
    required init(remark: String, vipaccount: String, payment: String, serialnumber: String, vipname: String, employeeid: String, createdat: String, depositmoney: String, storename: String){
        self.Remark = remark
        self.VipAccount = vipaccount
        self.Payment = payment
        self.SerialNumber = serialnumber
        self.VipName = vipname
        self.EmployeeID = employeeid
        self.CreatedAt = createdat
        self.DepositMoney = depositmoney
        self.StoreName = storename
    }
}
