//
//  ShoppingItem.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/19.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation

class ShoppingItem{
    var Id: Int
    var Store_number: String
    var Store_name: String
    var Serial_number: String
    var Billing_date: String
    var Vip_number: String
    var Vip_name: String
    var Depostit_pay: String
    var Total_price: String
    var Is_valid: String
    var Created_at: String
    var Updated_at: String

    required init(id: Int, store_number: String, store_name: String, serial_number: String, billing_date: String, number: String, name: String, depostit_pay: String, total_price: String, valid: String, created_at: String, update_at: String){
        self.Id = id
        self.Store_number = store_number
        self.Store_name = store_name
        self.Serial_number = serial_number
        self.Billing_date = billing_date
        self.Vip_number = number
        self.Vip_name = name
        self.Depostit_pay = depostit_pay
        self.Total_price = total_price
        self.Is_valid = valid
        self.Created_at = created_at
        self.Updated_at = update_at
    }
    
    
}
