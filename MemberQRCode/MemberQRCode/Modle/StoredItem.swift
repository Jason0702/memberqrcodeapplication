//
//  StoredItem.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/19.
//  Copyright © 2020 SJ. All rights reserved.
//

import Foundation

class StoredItem{
    var Id: Int
    var Vip_id: String
    var Store_number: String
    var Store_name: String
    var Serial_number: String
    var Vip_number: String
    var Vip_name: String
    var Cash_money: String
    var Depostit_pay: String
    var Work_place: String
    var Pos_class: String
    var Is_valid: String
    var Remark: String
    var Created_at: String
    var Updated_at: String

    required init(id: Int,vip_id: String, store_number: String, store_name: String, serial_number: String, number: String, name: String, cash_money: String, depostit_pay: String, work_place: String, pos_class: String, valid: String, remark: String, created_at: String, update_at: String){
        self.Id = id
        self.Vip_id = vip_id
        self.Store_number = store_number
        self.Store_name = store_name
        self.Serial_number = serial_number
        self.Vip_number = number
        self.Vip_name = name
        self.Cash_money = cash_money
        self.Depostit_pay = depostit_pay
        self.Work_place = work_place
        self.Pos_class = pos_class
        self.Is_valid = valid
        self.Remark = remark
        self.Created_at = created_at
        self.Updated_at = update_at
    }
}
