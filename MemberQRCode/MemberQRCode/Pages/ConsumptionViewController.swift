//
//  ConsumptionViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/16.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

//扣款

class ConsumptionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var ShopTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableData), name: .Shopreload, object: nil)
    }
    
    @objc func reloadTableData(){
        ShopTableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Shopping.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ConsumptionTableViewCell", for: indexPath) as? ConsumptionTableViewCell else {
        return UITableViewCell()
        }
        
        if Shopping.count == 0{
            cell.MemberLabel.text = ""
            cell.PhoneLabel.text = ""
            cell.MoneyLabel.text = ""
            cell.StoreLabel.text = ""
        }else{
            cell.MemberLabel.text = Shopping[indexPath.row].Vip_name
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd HH:mm"

            let date: Date? = dateFormatterGet.date(from: Shopping[indexPath.row].Created_at)
            print(dateFormatter.string(from: date!))
            
            
            
            cell.PhoneLabel.text = dateFormatter.string(from: date!)
            
            let Money = NSNumber(value: Int(Shopping[indexPath.row].Total_price)!)
            
            let numberFormatter = NumberFormatter()
            numberFormatter.usesGroupingSeparator = true //设置用组分隔
            numberFormatter.groupingSeparator = "," //分隔符号
            numberFormatter.groupingSize = 3  //分隔位数
            //格式化
            let format = numberFormatter.string(from: Money)!
            cell.MoneyLabel.text = "$"+format
            //cell.MoneyLabel.text = "$" + Shopping[indexPath.row].Total_price
            
            cell.StoreLabel.text = Shopping[indexPath.row].Store_name
        }
        
        return cell
        
    }
    
    

}
