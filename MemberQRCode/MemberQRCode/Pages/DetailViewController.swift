//
//  DetailViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/17.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

var textTarget: String! = ""

class DetailViewController: UIViewController, UIPopoverPresentationControllerDelegate,DiscoveryViewDelegate, Epos2PtrReceiveDelegate, UITextFieldDelegate{

    let PAGE_AREA_HEIGHT: Int = 500
    let PAGE_AREA_WIDTH: Int = 500
    let FONT_A_HEIGHT: Int = 24
    let FONT_A_WIDTH: Int = 12
    let BARCODE_HEIGHT_POS: Int = 70
    let BARCODE_WIDTH_POS: Int = 110
    
    // 接傳遞的值
    var GetQRCode: String = ""
    var GetEmployeeID: String = ""
    var GetEmployeeName: String = ""
    
    var keyBoardNeedLayout: Bool = true
    
    var UpDataDetail = NetWorkJob()
    

    @IBOutlet weak var BackButton: UIButton!
    
    @IBOutlet weak var ScrollView: UIScrollView!
    
    @IBOutlet weak var PayType: UISegmentedControl!
    
    @IBOutlet weak var MoneyField: UITextField!
    
    @IBOutlet weak var RemarkText: UITextField!
    
    @IBOutlet weak var PrintButton: UIButton!
    
    @IBOutlet weak var NOPrintButton: UIButton!
    
    
    var PayText: String?
    var EmployeeID: String?
    var Vip_Name: String?
    var Vip_Number: String?
    var CreatTime: String?
    var StoreName: String?
    var DepositeMoney: String?
    var TotalPrice: String?
    var WalletBalance: String?
    var LastWalletBalance: String?
    var PrintType: String?
    
    
    var printer: Epos2Printer?
    var valuePrinterSeries: Epos2PrinterSeries = EPOS2_TM_M10
    var valuePrinterModel: Epos2ModelLang = EPOS2_MODEL_TAIWAN
    /*鍵盤不遮擋對話筐*/
    func textFieldDidBeginEditing(_ textField: UITextField) {
        ScrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        ScrollView.setContentOffset(CGPoint(x: 0, y:0), animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PrintButton.addTarget(self, action: #selector(PrintButtonClick), for: .touchUpInside)
        NOPrintButton.addTarget(self, action: #selector(NoPrintButtonClick), for: .touchUpInside)
        
        BackButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        
        
        PayType.setTitleTextAttributes([.font: UIFont.systemFont(ofSize: 30) ], for: .normal)
        
        
        PayType.isUserInteractionEnabled = true
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap) // to Replace "TouchesBegan"
        
        NotificationCenter.default.addObserver(self, selector: #selector(PrintListfunc), name: .PrintList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NoPrintfunc), name: .NoPrint, object: nil)
        
        MoneyField.delegate = self
        MoneyField.keyboardType = .numberPad
        RemarkText.delegate = self
        
        MoneyField.text! = "100"
        PayText = "儲值"
    }
    
    
    @objc func PrintListfunc(notification: NSNotification){
        
        let info = notification.object
        
        print("PayType\(PayType.selectedSegmentIndex)")
        
            let printdata: Print = info as! Print
            print("print\(printdata)")
            PrintType = printdata.PayType
            EmployeeID = printdata.EmployeeID
            Vip_Name = printdata.VipName
            Vip_Number = printdata.VipAccount
            CreatTime = printdata.CreatedAt
            StoreName = printdata.StoreName
            DepositeMoney = printdata.DepositMoney
            TotalPrice = printdata.TotalPrice
            WalletBalance = printdata.WalletBalance
            LastWalletBalance = printdata.LastWalletBalance
        
    }
    @objc func NoPrintfunc(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            print("时间2：", Date())
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("is deinitialized")
    }
    
    @IBAction func SelectPayType(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0 {
            PayText = "儲值"
            MoneyField.text! = "100"
        }else {
            PayText = "扣款"
            MoneyField.text! = "0"
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        initializePrinterObject()
        
    }
    
     @objc func clickButton(){
           self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
       }
    
    @objc func PrintButtonClick(){
        if MoneyField.text!.isEmpty{
            SelectMoneyAlert()
            return
        }else if MoneyField.text! == "0"{
            ErrorMoneyFormatAlert()
            return
        }else if !MoneyField.text!.isEmpty && MoneyField.text != "0"{
            let regex =  "^[0-9]*$"
            if MoneyField.text!.range(of: regex, options: .regularExpression) == nil{
                ErrorMoneyFormatAlert()
                return
            }else{
                if PayText!.contains("扣款") && RemarkText.text!.isEmpty{
                    RemarkText.text! = "無"
                }else if PayText!.contains("儲值") && RemarkText.text!.isEmpty{
                    RemarkText.text! = "無"
                }
                if textTarget.isEmpty{
                    OpenConnect()
                }else{
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                        print("时间2：", Date())
                        self.updateButtonState(false)
                        if !self.runPrinterReceiptSequence() {
                            self.updateButtonState(true)
                        }
                    }
                     self.UploadList(isPrint: true)
                }
            }
        }
    }
    func OpenConnect(){
         let alertController = UIAlertController(title: "打印機錯誤！",
               message: "請先點右上角進入藍芽連線配對打印機", preferredStyle: .alert)
               
               let IKnowAction = UIAlertAction(title: "我知道了", style: .destructive, handler: {
                   action in
                   self.MoneyField.becomeFirstResponder()
               })
               
               alertController.addAction(IKnowAction)
               self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func NoPrintButtonClick(){
        if MoneyField.text!.isEmpty{
            SelectMoneyAlert()
            return
        }else if MoneyField.text! == "0"{
            ErrorMoneyFormatAlert()
            return
        }else if !MoneyField.text!.isEmpty && MoneyField.text != "0"{
            let regex =  "^[0-9]*$"
            if MoneyField.text!.range(of: regex, options: .regularExpression) == nil{
                ErrorMoneyFormatAlert()
                return
            }else{
                if PayText!.contains("扣款") && RemarkText.text!.isEmpty{
                    RemarkText.text! = "無"
                }else if PayText!.contains("儲值") && RemarkText.text!.isEmpty{
                    RemarkText.text! = "無"
                }
                self.UploadList(isPrint: false)
            }
        }
        
        
    }
    
    
    //上傳清單
    func UploadList(isPrint: Bool){
               let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "yyyy-MM-dd"
               let date = Date()
               let dateString = dateFormatter.string(from: date)
        DispatchQueue.main.async {
               if self.PayText!.contains("扣款"){
                self.UpDataDetail.POSTShoppingRecord(UserID: self.GetQRCode, DateTime: dateString, Price: self.MoneyField.text!, Remark: self.RemarkText.text!, EmployeeID: self.GetEmployeeID, isPrint: isPrint)
               }else if self.PayText!.contains("儲值"){
                self.UpDataDetail.POSTStoredRecord(UserID: self.GetQRCode, Price: self.MoneyField.text!,Remark: self.RemarkText.text!, EmployeeID: self.GetEmployeeID, isPrint: isPrint)
               }
        }
    }
    
    //請輸入金額
    func SelectMoneyAlert(){
        let alertController = UIAlertController(title: "請輸入金額",
        message: nil, preferredStyle: .alert)
        
        let IKnowAction = UIAlertAction(title: "我知道了", style: .destructive, handler: {
            action in
            self.MoneyField.becomeFirstResponder()
        })
        
        alertController.addAction(IKnowAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func ErrorMoneyFormatAlert(){
         let alertController = UIAlertController(title: "金額輸入錯誤！",
               message: nil, preferredStyle: .alert)
               
               let IKnowAction = UIAlertAction(title: "我知道了", style: .destructive, handler: {
                   action in
                   self.MoneyField.becomeFirstResponder()
               })
               
               alertController.addAction(IKnowAction)
               self.present(alertController, animated: true, completion: nil)
    }
    
    func updateButtonState(_ state: Bool) {
        PrintButton.isEnabled = state
        NOPrintButton.isEnabled = state
    }
    
    func runPrinterReceiptSequence() -> Bool {

        if !createReceiptData() {
            return false
        }
        
        if !printData() {
            return false
        }
        
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
        finalizePrinterObject()
    }
    
    @discardableResult
    func initializePrinterObject() -> Bool {
        printer = Epos2Printer(printerSeries: valuePrinterSeries.rawValue , lang: valuePrinterModel.rawValue)
        
        if printer == nil {
            return false
        }
        printer!.setReceiveEventDelegate(self)
        
        return true
    }
    
    func finalizePrinterObject() {
        if printer == nil {
            return
        }

        printer!.setReceiveEventDelegate(nil)
        printer = nil
    }
    
    
    func createReceiptData() -> Bool {
        
        var result = EPOS2_SUCCESS.rawValue
        
        let textData: NSMutableString = NSMutableString()

        //result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
        
        result = printer!.addTextAlign(EPOS2_ALIGN_LEFT.rawValue)
        
        //語系要設定
        printer?.addTextLang(EPOS2_LANG_ZH_TW.rawValue)
        
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"addTextAlign")
            return false;
        }
        
        // Section 1 : Store information
        
        result = printer!.addFeedLine(1)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            MessageView.showErrorEpos(result, method:"addFeedLine")
            return false
        }
        
        if PrintType == "儲值"{
            textData.append("會員儲值收據單\n\n")
        }
        if PrintType == "扣款"{
            textData.append("會員扣款收據單\n\n")
        }
        
        result = printer!.addText(textData as String)
    
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            MessageView.showErrorEpos(result, method:"addText")
            return false;
        }
        textData.setString("")
        
        // Section 2 : Purchaced items
        
        
        if result != EPOS2_SUCCESS.rawValue {
                MessageView.showErrorEpos(result, method:"addTextAlign")
                return false;
        }
               
        
        textData.append("店家:\(StoreName!)\n")
        textData.append("店員編號:\(EmployeeID!)\n")
        textData.append("會員:\(Vip_Number!) 姓名:\(Vip_Name!)\n")
        textData.append("儲值日期:\(CreatTime!) \n")
        textData.append("-----------------------------------------\n")
        
        if PrintType == "儲值"{
            textData.append("前次儲值金額:\(String(format: "%8@",LastWalletBalance!))\n")
            textData.append("本次儲值金額:\(String(format: "%8@",DepositeMoney!))\n")
            textData.append("剩餘儲值金額:\(String(format: "%8@",WalletBalance!))\n")
        }
        if PrintType == "扣款"{
            textData.append("消費前儲值金額:\(String(format: "%8@",LastWalletBalance!))\n")
            textData.append("本次付款金額:\(String(format: "%8@",TotalPrice!))\n")
            textData.append("剩餘儲值金額:\(String(format: "%8@",WalletBalance!))\n")
        }
            
        textData.append("-----------------------------------------\n")
        
        result = printer!.addText(textData as String)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            MessageView.showErrorEpos(result, method:"addText")
            return false;
        }
        textData.setString("")
        
        // Section 3:
        textData.append("會員簽名:_____________________\n")
        textData.append("-----------------------------------------\n")
        
        result = printer!.addText(textData as String)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            MessageView.showErrorEpos(result, method:"addText")
            return false;
        }
        textData.setString("")
        
        result = printer!.addCut(EPOS2_CUT_FEED.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            MessageView.showErrorEpos(result, method:"addCut")
            return false
        }
        
        return true
    }
    
    func printData() -> Bool {
        if printer == nil {
            return false
        }
        
        if !connectPrinter() {
            printer!.clearCommandBuffer()
            return false
        }
        
        let result = printer!.sendData(Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            MessageView.showErrorEpos(result, method:"sendData")
            printer!.disconnect()
            return false
        }
        
        return true
    }
    
    func connectPrinter() -> Bool {
        var result: Int32 = EPOS2_SUCCESS.rawValue
        
        if printer == nil {
            return false
        }
        result = printer!.connect(textTarget, timeout:Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            MessageView.showErrorEpos(result, method:"connect")
            return false
        }
        
        return true
    }
    
     func onPtrReceive(_ printerObj: Epos2Printer!, code: Int32, status: Epos2PrinterStatusInfo!, printJobId: String!) {
        if code == 0{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
           MessageView.showResult(code, errMessage: makeErrorMessage(status))
           
           dispPrinterWarnings(status)
           updateButtonState(true)
        
           DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: {
               //self.disconnectPrinter()
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
           })
       }
    
    func disconnectPrinter() {
        var result: Int32 = EPOS2_SUCCESS.rawValue
        
        if printer == nil {
            return
        }
        
        result = printer!.disconnect()
        if result != EPOS2_SUCCESS.rawValue {
            DispatchQueue.main.async(execute: {
                MessageView.showErrorEpos(result, method:"disconnect")
            })
        }

        printer!.clearCommandBuffer()
    }
    
    
    func dispPrinterWarnings(_ status: Epos2PrinterStatusInfo?) {
        if status == nil {
            return
        }
        
        //textWarnings.text = ""
        
        if status!.paper == EPOS2_PAPER_NEAR_END.rawValue {
            //textWarnings.text = NSLocalizedString("warn_receipt_near_end", comment:"")
        }
        
        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_1.rawValue {
            //textWarnings.text = NSLocalizedString("warn_battery_near_end", comment:"")
        }
    }

    func makeErrorMessage(_ status: Epos2PrinterStatusInfo?) -> String {
        let errMsg = NSMutableString()
        if status == nil {
            return ""
        }
    
        if status!.online == EPOS2_FALSE {
            errMsg.append(NSLocalizedString("err_offline", comment:""))
        }
        if status!.connection == EPOS2_FALSE {
            errMsg.append(NSLocalizedString("err_no_response", comment:""))
        }
        if status!.coverOpen == EPOS2_TRUE {
            errMsg.append(NSLocalizedString("err_cover_open", comment:""))
        }
        if status!.paper == EPOS2_PAPER_EMPTY.rawValue {
            errMsg.append(NSLocalizedString("err_receipt_end", comment:""))
        }
        if status!.paperFeed == EPOS2_TRUE || status!.panelSwitch == EPOS2_SWITCH_ON.rawValue {
            errMsg.append(NSLocalizedString("err_paper_feed", comment:""))
        }
        if status!.errorStatus == EPOS2_MECHANICAL_ERR.rawValue || status!.errorStatus == EPOS2_AUTOCUTTER_ERR.rawValue {
            errMsg.append(NSLocalizedString("err_autocutter", comment:""))
            errMsg.append(NSLocalizedString("err_need_recover", comment:""))
        }
        if status!.errorStatus == EPOS2_UNRECOVER_ERR.rawValue {
            errMsg.append(NSLocalizedString("err_unrecover", comment:""))
        }
    
        if status!.errorStatus == EPOS2_AUTORECOVER_ERR.rawValue {
            if status!.autoRecoverError == EPOS2_HEAD_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_head", comment:""))
            }
            if status!.autoRecoverError == EPOS2_MOTOR_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_motor", comment:""))
            }
            if status!.autoRecoverError == EPOS2_BATTERY_OVERHEAT.rawValue {
                errMsg.append(NSLocalizedString("err_overheat", comment:""))
                errMsg.append(NSLocalizedString("err_battery", comment:""))
            }
            if status!.autoRecoverError == EPOS2_WRONG_PAPER.rawValue {
                errMsg.append(NSLocalizedString("err_wrong_paper", comment:""))
            }
        }
        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_0.rawValue {
            errMsg.append(NSLocalizedString("err_battery_real_end", comment:""))
        }
    
        return errMsg as String
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DiscoveryView" {
            let view: PopUpWindowViewController? = segue.destination as? PopUpWindowViewController
            view?.delegate = self
        }
    }
    func discoveryView(_ sendor: PopUpWindowViewController, onSelectPrinterTarget target: String) {
        textTarget = target
        print("Target: \(target)")
    }
    
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
}
