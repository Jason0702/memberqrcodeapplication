//
//  HomeViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/12.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

var RealName: String! = ""

class HomeViewController: UIViewController{
    
    @IBOutlet weak var QRcodeView: UIView!
    
    @IBOutlet weak var SearchView: UIView!
    
    @IBOutlet weak var MemberView: UIView!
    
    @IBOutlet weak var RealNameLabel: UILabel!
    
    @IBOutlet weak var QRcodeBeforeBtn: UIButton!
    
    @IBOutlet weak var QRcodeAfterBtn: UIButton!
    
    @IBOutlet weak var QRcodeLabel: UILabel!
    
    @IBOutlet weak var SearchBeforeBtn: UIButton!
    
    @IBOutlet weak var SearchAfterBtn: UIButton!
    
    @IBOutlet weak var SearchLabel: UILabel!
    
    @IBOutlet weak var MemberBeforeBtn: UIButton!
    
    @IBOutlet weak var MemberAfterBtn: UIButton!
    
    @IBOutlet weak var MemberLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ViewClick()
        RealNameLabel.text = "目前登入店家為："+RealName
        
    }
    
    func ViewClick(){
        let QRgesture = UITapGestureRecognizer(target: self, action:  #selector(self.QRcheckAction))
        let Searchgesture = UITapGestureRecognizer(target: self, action:  #selector(self.SearchcheckAction))
        let Membergesture = UITapGestureRecognizer(target: self, action:  #selector(self.MembercheckAction))
        
        self.QRcodeView.addGestureRecognizer(QRgesture)
        self.QRcodeLabel.addGestureRecognizer(QRgesture)
        self.QRcodeLabel.isUserInteractionEnabled = true
        self.QRcodeAfterBtn.addTarget(self, action: #selector(self.QRcheckAction), for: .touchUpInside)
        self.QRcodeBeforeBtn.addTarget(self, action: #selector(self.QRcheckAction), for: .touchUpInside)
        
        self.SearchView.addGestureRecognizer(Searchgesture)
        self.SearchLabel.addGestureRecognizer(Searchgesture)
        self.SearchLabel.isUserInteractionEnabled = true
        self.SearchAfterBtn.addTarget(self, action: #selector(self.SearchcheckAction), for: .touchUpInside)
        self.SearchBeforeBtn.addTarget(self, action: #selector(self.SearchcheckAction), for: .touchUpInside)
        
        self.MemberView.addGestureRecognizer(Membergesture)
        self.MemberLabel.addGestureRecognizer(Membergesture)
        self.MemberLabel.isUserInteractionEnabled = true
        self.MemberAfterBtn.addTarget(self, action: #selector(self.MembercheckAction), for: .touchUpInside)
        self.MemberBeforeBtn.addTarget(self, action: #selector(self.MembercheckAction), for: .touchUpInside)
    }
    

    @objc func QRcheckAction(sender : UITapGestureRecognizer) {
        print("TapQR")
        GOPages(Page: "QRCodeViewController")
    }
    @objc func SearchcheckAction(sender : UITapGestureRecognizer) {
        print("TapSearch")
        GOPages(Page: "TransActionViewController")
    }
    @objc func MembercheckAction(sender : UITapGestureRecognizer) {
        print("TapMember")
       GOPages(Page: "MemberViewController")
        
    }
    func GOPages(Page: String){
         if let controller = storyboard?.instantiateViewController(withIdentifier: Page) {
                   present(controller, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
