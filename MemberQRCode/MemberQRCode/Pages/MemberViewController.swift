//
//  MemberViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/12.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

class MemberViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate{
    
    let NetJob = NetWorkJob()
    
    var filteredData: [MemberItem]! = []
    
    @IBOutlet weak var BackButton: UIButton!
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    @IBOutlet weak var TableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        BackButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        
        // 搜索内容为空时，显示全部内容
        self.SearchBar.delegate = self
        self.SearchBar.placeholder = "電話搜尋"
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async(execute: {
            self.NetJob.GetMember()
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableData), name: .reload, object: nil)
    }
    @objc func reloadTableData(){
        filteredData = Member
        TableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func clickButton(){
        Member.removeAll()
        filteredData.removeAll()
        dismiss(animated: true, completion: nil)
    }
    
    
       
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("[ViewController searchBar] searchText: \(searchText)")
    
        filteredData = searchText.isEmpty ? Member : Member.filter({(member: MemberItem) -> Bool in
            return member.Phone.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        TableView.reloadData()
        
    }
     
    // 搜索触发事件，点击虚拟键盘上的search按钮时触发此方法
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        print("搜尋 \(searchBar.text)")
    }
     
    // 书签按钮触发事件
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        print("搜索历史")
    }
     
    // 取消按钮触发事件
     
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // 搜索内容置空
        searchBar.text = ""
    }
    
    
    //TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MemberTableViewCell", for: indexPath) as? MemberTableViewCell else {
        return UITableViewCell()
        }
        
        cell.DateTimeLabel.text = filteredData[indexPath.row].DateTime
        cell.PhoneLabel.text = filteredData[indexPath.row].Phone
        cell.MemberLabel.text = filteredData[indexPath.row].Member
        
        
        let Money = NSNumber(value: Int(filteredData[indexPath.row].Money)!)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true //设置用组分隔
        numberFormatter.groupingSeparator = "," //分隔符号
        numberFormatter.groupingSize = 3  //分隔位数
        //格式化
        let format = numberFormatter.string(from: Money)!
        cell.MoneyLabel.text = "$"+format
        //cell.MoneyLabel.text =  "$" + filteredData[indexPath.row].Money
        return cell
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let Numbering = UILabel()
        Numbering.backgroundColor = UIColor(rgb: 0x888888)
        Numbering.text = "編號"
        Numbering.textColor = UIColor.white
        Numbering.textAlignment = .center
        Numbering.font = Numbering.font.withSize(30)

        let VipName = UILabel()
        VipName.backgroundColor = UIColor(rgb: 0x888888)
        VipName.text = "會員"
        VipName.textColor = UIColor.white
        VipName.textAlignment = .center
        VipName.font = VipName.font.withSize(30)
        
        let Phone = UILabel()
        Phone.backgroundColor = UIColor(rgb: 0x888888)
        Phone.text = "電話"
        Phone.textColor = UIColor.white
        Phone.textAlignment = .center
        Phone.font = Phone.font.withSize(30)
        
        let Money = UILabel()
        Money.backgroundColor = UIColor(rgb: 0x888888)
        Money.text = "餘額"
        Money.textColor = UIColor.white
        Money.textAlignment = .center
        Money.font = Money.font.withSize(30)

        let mStackView = UIStackView()
        mStackView.axis = NSLayoutConstraint.Axis.horizontal
        mStackView.distribution  = UIStackView.Distribution.fillProportionally
        mStackView.alignment = UIStackView.Alignment.center
        //mStackView.spacing = 10

        let margins = mStackView.layoutMarginsGuide
        mStackView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 2).isActive = true
        mStackView.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 2).isActive = true
        mStackView.topAnchor.constraint(equalTo: margins.topAnchor, constant: 2).isActive = true
        mStackView.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: 2).isActive = true

        mStackView.addArrangedSubview(Numbering)
        mStackView.addArrangedSubview(VipName)
        mStackView.addArrangedSubview(Phone)
        mStackView.addArrangedSubview(Money)
        
        
        return mStackView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
  
    
    
}


//Color
extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
