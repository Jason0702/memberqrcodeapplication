//
//  QRCodeViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/12.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var camView: UIView!
    
    var captureSesion: AVCaptureSession?
    var previewLayer: AVCaptureVideoPreviewLayer!
    var QRCodeString: String!

    
    @IBOutlet weak var BackButton: UIButton!
    
    let NetJob = NetWorkJob()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        BackButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(EmployeeNumberCheck), name: .EmployeeEntry, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(captureSesion?.isRunning == false){
            captureSesion?.startRunning()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setQRCodeScan()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (context) -> Void in
            self.previewLayer?.connection?.videoOrientation = self.transformOrientation(orientation: UIInterfaceOrientation(rawValue: UIApplication.shared.statusBarOrientation.rawValue)!)
        }, completion: { (context) -> Void in
            
        })
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    @objc func clickButton(){
        dismiss(animated: true, completion: nil)
    }
    
    //掃QRCode的動作
    func setQRCodeScan(){
        
        //實體化一個AVCaptureSession物件
        captureSesion = AVCaptureSession()
        
        //AVCaptureDevice可以抓到相機和其屬性
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else{return}
        
        let videoInput: AVCaptureDeviceInput
        do{
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        }catch let error{
            print(error)
            return
        }
        if (captureSesion?.canAddInput(videoInput) ?? false){
            captureSesion?.addInput(videoInput)
        }else{
            return
        }
    //AVCaptureMetaDataOutput輸出影音資料，先實體化AVCaptureMetaDataOutput物件
        let metaDataOutput = AVCaptureMetadataOutput()
        if(captureSesion?.canAddOutput(metaDataOutput) ?? false){
            captureSesion?.addOutput(metaDataOutput)
            
            //關鍵！執行處理QRCode
            metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            //metadataOutput.metadataObjectTypes表示要處理哪些類型的資料，處理QRCODE
            metaDataOutput.metadataObjectTypes = [.qr, .ean8 , .ean13 , .pdf417, .code39]
        }else{
            return
        }
        //用AVCaptureVideoPreviewLayer來呈現Session上的資料
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSesion!)
        //顯示size
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = self.transformOrientation(orientation: UIInterfaceOrientation(rawValue: UIApplication.shared.statusBarOrientation.rawValue)!)
        //呈現在camView上面
        var frame = camView.layer.bounds
        frame.origin.y = 110
        previewLayer.frame = frame
        //加入畫面
        view.layer.addSublayer(previewLayer)
         
        //顯示scan Area window 框框
        let size = 300
        let sWidth = Int(view.frame.size.width)
        let xPos = (sWidth/2)-(size/2)
        let scanRect = CGRect(x: CGFloat(xPos), y: frame.origin.y+100 , width: CGFloat(size) , height: CGFloat(size))
        //設定scan Area window 框框
        let scanAreaView = UIView()
        scanAreaView.layer.borderColor = UIColor.gray.cgColor
        scanAreaView.layer.borderWidth = 2
        scanAreaView.frame = scanRect
        view.addSubview(scanAreaView)
        view.bringSubviewToFront(scanAreaView)
                
        //開始影像擷取呈現鏡頭的畫面
        captureSesion?.startRunning()
        
    }
    
    //使用AVCaptureMetadataOutput物件辨識QR Code，此AVCaptureMetadataOutputObjectsDelegate的委派方法metadataOutout會被呼叫
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSesion?.startRunning()
        if let metadataObject = metadataObjects.first{
            
            //AVMetadataMachineReadableCodeObject是從OutPut擷取到barcode內容
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else {return}
            //將讀取到的內容轉成string
            guard let stringValue = readableObject.stringValue else {return}
            
            //掃到QRCode後的震動提示
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            //將string資料放到label元件上
            //codeTextLabel.text = stringValue
            AlertController(sendText: stringValue)
            print(stringValue)
            
            //存取QRcodeURL
            QRCodeString = stringValue
            
        }
    }
    
     //畫面不顯示即停止掃描
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if(captureSesion?.isRunning == true){
            captureSesion?.stopRunning()
        }
    }

    func transformOrientation(orientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch orientation {
        case .landscapeLeft:
            return .landscapeLeft
        case .landscapeRight:
            return .landscapeRight
        case .portraitUpsideDown:
            return .portraitUpsideDown
        default:
            return .portrait
        }
    }
    
    //對話框
    func AlertController(sendText: String){
        
        if(captureSesion?.isRunning == true){
            captureSesion?.stopRunning()
        }
        
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "ArialHebrew-Bold", size: 27.0)!]
        let titleAttrString = NSMutableAttributedString(string: "員工編號", attributes: titleFont)
        
        let alertController = UIAlertController(title: "",
                        message: nil, preferredStyle: .alert)
        
        alertController.addTextField{
                   (TextField: UITextField!) -> Void in
                   TextField.delegate = self
                   TextField.addConstraint(TextField.heightAnchor.constraint(equalToConstant: 30))
                   TextField.font = UIFont(name: "Helvetica", size: 27)
                   TextField.placeholder = "請輸入員工編號"
               }
        
        alertController.setValue(titleAttrString, forKey: "attributedTitle")
        
        let StoredValueAction = UIAlertAction(title: "確認", style: .default, handler: {
            action in
            
            let EmployeeID = (alertController.textFields?.first)! as UITextField
            if !EmployeeID.text!.isEmpty{
                self.NetJob.GetEmployeeCheck(EmployeeID: EmployeeID.text!)
                if(self.captureSesion?.isRunning != true){
                    self.captureSesion?.startRunning()
                }
            }else{
                self.ErrorMsgAlertController()
                if(self.captureSesion?.isRunning != true){
                    self.captureSesion?.startRunning()
                }
            }
        })
        
        
        
        let CancelAction = UIAlertAction(title: "取消", style: .destructive, handler: {
            action in
            if(self.captureSesion?.isRunning != true){
                self.captureSesion?.startRunning()
            }
        })
        
        alertController.addAction(StoredValueAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func EmployeeNumberCheck(){
        if !EmployeeUser.isEmpty {
            for(key,value) in EmployeeUser{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let controller = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController{
                        controller.GetQRCode = QRCodeString
                        controller.GetEmployeeID = key
                        self.present(controller, animated: true, completion: nil)
                }
            }
        }else{
            self.ErrorMsgAlertController()
        }
    }
    
    
    func ErrorMsgAlertController(){
        let alertController = UIAlertController(title: "輸入錯誤,請確認編號是否正確！",
        message: nil, preferredStyle: .alert)
        let ConfirmAction = UIAlertAction(title: "知道了！", style: .default, handler: nil)
        alertController.addAction(ConfirmAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
