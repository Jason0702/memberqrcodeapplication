//
//  StoredValueViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/16.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

//儲值
class StoredValueViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var StoreTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableData), name: .Storeload, object: nil)
    }
    
    @objc func reloadTableData(){
        StoreTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Stored.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StoredValueTableViewCell", for: indexPath) as? StoredValueTableViewCell else {
        return UITableViewCell()
        }
        if Stored.count == 0{
            cell.MemberLabel.text = ""
            cell.PhoneLabel.text = ""
            cell.MoneyLabel.text = ""
            cell.StoreLabel.text = ""
        }else{
            
            cell.MemberLabel.text = Stored[indexPath.row].Vip_name
            
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd HH:mm"

            let date: Date? = dateFormatterGet.date(from: Stored[indexPath.row].Created_at)
            
            cell.PhoneLabel.text = dateFormatter.string(from: date!)
            
            
            let Money = NSNumber(value: Int(Stored[indexPath.row].Depostit_pay)!)
            
            let numberFormatter = NumberFormatter()
            numberFormatter.usesGroupingSeparator = true //设置用组分隔
            numberFormatter.groupingSeparator = "," //分隔符号
            numberFormatter.groupingSize = 3  //分隔位数
            //格式化
            let format = numberFormatter.string(from: Money)!
            cell.MoneyLabel.text = "$"+format
            
            //cell.MoneyLabel.text = "$" + Stored[indexPath.row].Depostit_pay
            cell.StoreLabel.text = Stored[indexPath.row].Store_name
        }
        return cell
        
    }
    
    
}
