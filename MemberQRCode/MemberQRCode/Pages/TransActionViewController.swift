//
//  TransActionViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/12.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

class TransActionViewController: UIViewController, UITextFieldDelegate, UISearchBarDelegate{
   
    @IBOutlet weak var BackButton: UIButton!
    
    @IBOutlet weak var StartDateTimeText: UITextField!
    
    @IBOutlet weak var EndDateTimeText: UITextField!
    
    @IBOutlet var ContainerView: [UIView]!
    
    @IBOutlet var Segmented: UISegmentedControl!
    
    @IBOutlet weak var SeachButton: UIButton!
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    
    // 创建一个日期格式器
    let dformatter = DateFormatter()
    // 上傳用日期
    let Updformatter = DateFormatter()
    
    let NetJob = NetWorkJob()
    
    var Account: String! = ""
    var UpStartDate: String!
    var UpEndDate: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async(execute: {
            self.NetJob.GetShoppingRecord()
            self.NetJob.GetStoredRecord()
        })
        
        self.SearchBar.delegate = self
        self.SearchBar.placeholder = "電話搜尋"
        
        BackButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
        
        StartDateTimeSetting()
        EndDateTimeSetting()
        
        DateTimeSetting()
        
        ActivityStart()
        
        SeachButton.addTarget(self, action: #selector(SeachClick), for: .touchUpInside)
    }
    
    
    
    
    func StartDateTimeSetting(){
        StartDateTimeText.layer.borderColor = UIColor.blue.cgColor
        StartDateTimeText.layer.borderWidth = 1
        StartDateTimeText.layer.cornerRadius = 6
    }
    func EndDateTimeSetting(){
        EndDateTimeText.layer.borderColor = UIColor.blue.cgColor
        EndDateTimeText.layer.borderWidth = 1
        EndDateTimeText.layer.cornerRadius = 6
    }
    
    func DateTimeSetting(){
        // 为日期格式器设置格式字符串
        dformatter.dateFormat = "yyyy年MM月dd日"
        Updformatter.dateFormat = "yyyy-MM-dd"
        
        // 建立一個 UIDatePicker
        var StartDate = UIDatePicker()
        let EndDate = UIDatePicker()
        
        // 設置 UIDatePicker 格式
        StartDate.datePickerMode = .date
        EndDate.datePickerMode = .date
        
        // 設置 UIDatePicker 顯示的語言環境
        StartDate.locale = Locale(identifier: "zh_TW")
        EndDate.locale = Locale(identifier: "zh_TW")
        
        // 設置 UIDatePicker 預設日期為現在日期
        StartDate.date = Date()
        EndDate.date = Date()
        
        // 設置 UIDatePicker 改變日期時會執行動作的方法
        StartDate.addTarget(self, action: #selector(datePickerChanged), for: .valueChanged)
        EndDate.addTarget(self, action: #selector(datePickerChangedEnd), for: .valueChanged)
        
        StartDateTimeText.text = dformatter.string(from: StartDate.date)
        EndDateTimeText.text = dformatter.string(from: EndDate.date)
        UpStartDate = Updformatter.string(from: StartDate.date)
        UpEndDate = Updformatter.string(from: EndDate.date)
        
        
        StartDateTimeText.autocorrectionType = .no
        EndDateTimeText.autocorrectionType = .no
        StartDateTimeText.inputView = StartDate
        EndDateTimeText.inputView = EndDate
        
    }
    
    func ActivityStart(){
        ContainerView[0].isHidden = false
        ContainerView[1].isHidden = true
        
        let font = UIFont.systemFont(ofSize: 27)
        Segmented.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
    }
    
    @IBAction func changeCat(_ sender: UISegmentedControl) {
        ContainerView.forEach {
           $0.isHidden = true
        }
        ContainerView[sender.selectedSegmentIndex].isHidden = false
    }
    
    
    
    // UIDatePicker 改變選擇時執行的動作
     @objc func datePickerChanged(datePicker:UIDatePicker) {
         // 將 UITextField 的值更新為新的日期
         StartDateTimeText.text = dformatter.string(from: datePicker.date)
        UpStartDate = Updformatter.string(from: datePicker.date)
     }
    @objc func datePickerChangedEnd(datePicker: UIDatePicker){
        EndDateTimeText.text = dformatter.string(from: datePicker.date)
        UpEndDate = Updformatter.string(from: datePicker.date)
    }
    
    @objc func clickButton(){
        Stored.removeAll()
        Shopping.removeAll()
        dismiss(animated: true, completion: nil)
    }
    @objc func SeachClick(){
        
        if Account.isEmpty{
            print("Date \(UpStartDate)  \(UpEndDate)")
            NetJob.DateGetShoppingRecord(StartDate: UpStartDate, EndDate: UpEndDate)
            NetJob.DateGetStoreRecord(StartDate: UpStartDate, EndDate: UpEndDate)
        }else{
            print("AccountDate")
            NetJob.AccountDateGetShoppingRecord(Account: Account, StartDate: UpStartDate, EndDate: UpEndDate)
            NetJob.AccountDateGetStoreRecord(Account: Account, StartDate: UpStartDate, EndDate: UpEndDate)
        }
    }
    
    //鍵盤收起
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func dismissKeyBoard(){
        self.view.endEditing(true)
    }
    
    //SearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        print("[ViewController searchBar] searchText: \(searchText)")
        Account = searchText
    }
    // 搜索触发事件，点击虚拟键盘上的search按钮时触发此方法
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        print("搜尋 \(searchBar.text!)")
    }
    
    // 书签按钮触发事件
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        print("搜索历史")
    }
     
    // 取消按钮触发事件
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // 搜索内容置空
        searchBar.text = ""
    }
    
}
