//
//  ViewController.swift
//  MemberQRCode
//
//  Created by SJ on 2020/3/12.
//  Copyright © 2020 SJ. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate{
    
    @IBOutlet weak var LoginButton: UIButton!
    
    @IBOutlet weak var UserAccount: UITextField!
    @IBOutlet weak var UserPassWord: UITextField!
    
    @IBOutlet weak var CheckButton: UIButton!
    
    var Account: String!
    var PassWord: String!
    var isSave: Bool! = false
    
    var NetShopLogin = NetWorkJob()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap) // to Replace "TouchesBegan"
        
        
        ButtonSetting()
        AccountSetting()
        PassWordSetting()
        loadUser()
    }
    
    func AccountSetting(){
        UserAccount.delegate = self
        UserAccount.placeholder = "請輸入帳號"
        UserAccount.returnKeyType = .next
        
        UserAccount.layer.borderColor = UIColor.blue.cgColor
        UserAccount.layer.borderWidth = 1
        UserAccount.layer.cornerRadius = 6
        
    }
    func PassWordSetting(){
        UserPassWord.delegate = self
        UserPassWord.placeholder = "請輸入密碼"
        UserPassWord.isSecureTextEntry = true
        UserPassWord.returnKeyType = .done
        
        UserPassWord.layer.borderColor = UIColor.blue.cgColor
        UserPassWord.layer.borderWidth = 1
        UserPassWord.layer.cornerRadius = 6
    }
    func ButtonSetting(){
        LoginButton.layer.cornerRadius = 5.0
        LoginButton.layer.shadowColor = UIColor.black.cgColor
        LoginButton.layer.shadowRadius = 2
        LoginButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        LoginButton.layer.shadowOpacity = 0.3
        LoginButton.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
    }
    
    //Animate
    @IBAction func checkBoxTapped(_ sender: UIButton){
        UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }){ (success) in
            sender.isSelected = !sender.isSelected
            UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveLinear, animations: {
                sender.transform = .identity
                print("Save\(sender.isSelected)")
                self.isSave = sender.isSelected
            }, completion: nil)}
    }
    @IBAction func LoginButtonTapped(_ sender: UIButton){
        UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveEaseInOut, animations: {
                   sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
               }){ (success) in
                UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveEaseInOut, animations: {
                    sender.transform = .identity
                   }, completion: nil)}
    }
    
    //鍵盤收起
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case UserAccount:
            UserPassWord.becomeFirstResponder()
        default:
            self.view.endEditing(true)
        }
        return false
    }
    //
    @objc func clickButton(){
        if !UserAccount.text!.isEmpty{
            if !UserPassWord.text!.isEmpty{
                do{
                 try NetShopLogin.ShopLogin(Account: UserAccount.text!, Password: UserPassWord.text!, isSave: isSave)
                }catch let error{
                    print(error)
                }
            }else{
                AlertController(sendText: "密碼不得為空")
            }
        }else{
            AlertController(sendText: "帳號不得為空")
        }
    }
    
    //對話框
    func AlertController(sendText: String){
        let alertController = UIAlertController(title: "提示",
                        message: sendText, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "知道了", style: .default, handler: {
            action in
        })
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func loadUser(){
        let ud: UserDefaults = UserDefaults.standard
        if let Account = ud.value(forKey: "Account") as? String{
            UserAccount.text = Account
        }
        if let isSave = ud.value(forKey: "isSave") as? Bool{
            print("isSave\(isSave)")
            CheckButton.isSelected = isSave
            self.isSave = isSave
            if isSave{
                if let Password = ud.value(forKey: "Password") as? String{
                    UserPassWord.text = Password
                }
            }
        }
    }
}

