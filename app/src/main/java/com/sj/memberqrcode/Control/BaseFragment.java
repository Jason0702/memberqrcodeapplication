package com.sj.memberqrcode.Control;

import android.view.View;

import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {
    /**
     * Fragment當前狀態是否可見
     */
    public  boolean isVisible;
    /**
     *  inflate 布局文件 返回的View
     */
    public View mView;
    /**
     *  簡化 findViewById
     *
     * @param  viewId
     * @param  <T>
     * @return
     */
    protected <T extends View> T find(int viewId){
        return (T)mView.findViewById(viewId);
    }
    /**
     * setUserVisibleHint是在onCreateView之前調用的
     * 設置Framgent可見狀態
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //判斷是否可見
        if(getUserVisibleHint()){
            isVisible = true;
            onVisible();
        }else{
            isVisible = false;
            onInvisible();
        }
    }
    /**
     * 可見
     */
    private void onVisible(){
        lazyLoad();
    }
    /**
     * 不可見
     */
    private void onInvisible(){
    }

    /**
     * 延遲加載
     * 子類必須重寫此方法
     */
    public abstract void lazyLoad();
}
