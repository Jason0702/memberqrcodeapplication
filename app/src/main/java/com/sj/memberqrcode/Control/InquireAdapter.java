package com.sj.memberqrcode.Control;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sj.memberqrcode.Model.Detail_Item;
import com.sj.memberqrcode.R;

import java.util.List;

public class InquireAdapter extends RecyclerView.Adapter<InquireAdapter.ViewHolder> {
    private List<Detail_Item> detailItems;

    public InquireAdapter(List<Detail_Item> data){
        detailItems = data;
    }

    @NonNull
    @Override
    public InquireAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inquire_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.DateTimeText.setText(detailItems.get(position).getDateTime());
        holder.MoneyText.setText("$"+detailItems.get(position).getMoney());
        holder.NoteVext.setText(detailItems.get(position).getNote());
    }

    @Override
    public int getItemCount() {
        return detailItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView  DateTimeText, MoneyText, NoteVext;
        ViewHolder(View itemView){
            super(itemView);
            DateTimeText = itemView.findViewById(R.id.DateTimeTextView);
            MoneyText = itemView.findViewById(R.id.MoneyTextView);
            NoteVext = itemView.findViewById(R.id.NoteTextView);
        }
    }
}
