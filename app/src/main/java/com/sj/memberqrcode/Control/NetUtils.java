package com.sj.memberqrcode.Control;

import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NetUtils {

    final String TAG = "NetUtils";
    private String Url = "http://dev.twgo.asia/member-qrcode/";
    public String LoginURL = Url+"api/v1/vip/login";
    public String RegisteredURL = Url+"api/v1/vip";
    public String GetMemberURL = Url+"api/v1/vip-self/";//要加ID
    public String GetShoppingRecord = Url+"api/v1/vip-shopping-record/";//要加ID
    public String GetStoredRecord = Url+"api/v1/vip-stored-record/";//要加ID



    private static final byte[] LOCKER = new byte[0];
    private static NetUtils mInstance;
    private OkHttpClient mOkhttpClient;

    private NetUtils(){
        okhttp3.OkHttpClient.Builder ClientBuilder = new okhttp3.OkHttpClient.Builder();
        ClientBuilder.readTimeout(20, TimeUnit.SECONDS);//讀取超時
        ClientBuilder.connectTimeout(15,TimeUnit.SECONDS);//連結超時
        ClientBuilder.writeTimeout(60,TimeUnit.SECONDS);//寫入超時
        ClientBuilder.authenticator((route, response) -> {
            if(responseCount(response) >= 3){
                return null;
            }
            return response.request().newBuilder().header("Authentication",Util.Token).build();
        });
        mOkhttpClient = ClientBuilder.build();
    }
    private int responseCount(Response response){
        int result = 1;
        while ((response = response.priorResponse())!=null){
            result++;
        }
        return result;
    }
    /**
     * 單例模式獲取NetUtils
     * @return
     */
    public static NetUtils getInstance() {
        if (mInstance == null) {
            synchronized (LOCKER) {
                if (mInstance == null) {
                    mInstance = new NetUtils();
                }
            }
        }
        return mInstance;
    }
    /**
     * GET請求，同步方式
     * @param  url
     * @return
     */
    public Response getDataSynFromNet(String url){
        //1建構Request
        Request.Builder builder = new Request.Builder();
        Request request = builder.get().url(url).build();
        //2將Request封裝Call
        Call call = mOkhttpClient.newCall(request);
        //3執行Call,得到reponse
        Response response = null;
        try{
            response = call.execute();
        }catch (IOException e){
            e.printStackTrace();
        }
        return response;
    }
    /**
     * POST請求，同步方式
     * @param url
     * @param bodyParams
     * @return
     */
    public Response postDataSynToNet(String url, Map<String,String> bodyParams){
        //1構造RequestBody
        RequestBody body = setRequestBody(bodyParams);
        //2構造Request
        Request.Builder requestBuilder = new Request.Builder();
        Request request = requestBuilder.post(body).url(url).build();
        //3將Request封裝為Call
        Call call = mOkhttpClient.newCall(request);
        Response response = null;
        try{
            response = call.execute();
        }catch (IOException e){
            e.printStackTrace();
        }
        return response;
    }
    /**
     * 自定義網路回調接口
     */
    public interface MyNetCall{
        void success(Call call,Response response) throws IOException;
        void faild(Call call, IOException e);
    }
    /**
     * GET請求，異步
     * @param url
     * @param myNetCall
     * @return
     */
    public void getDataAsynFromNet(String url,final MyNetCall myNetCall){
        //1構造Request
        Request.Builder builder = new Request.Builder();
        builder.addHeader("Authentication",Util.Token);
        Request request = builder.get().url(url).build();
        //2將Request封裝為Call
        Call call = mOkhttpClient.newCall(request);
        //3執行Call
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                myNetCall.faild(call,e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                myNetCall.success(call, response);
            }
        });
    }

    /**
     * PUT請求，異步
     * @param url
     * @param bodyParams
     * @param myNetCall
     */
    public void putDataAsynToNet(String url,Map<String,String> bodyParams, final MyNetCall myNetCall){
        //1構造RequestBody
        RequestBody body = setRequestBody(bodyParams);
        //2構造Request
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.addHeader("Authentication",Util.Token);
        Request request = requestBuilder.put(body).url(url).build();
        //3將Request封裝為Call
        Call call = mOkhttpClient.newCall(request);
        //4執行Call
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                myNetCall.faild(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                myNetCall.success(call, response);
            }
        });
    }

    /**
     * POST請求，異步
     * @param url
     * @param bodyParams
     * @param myNetCall
     */
    public void postDataAsynToNet(String url,Map<String,String> bodyParams, final MyNetCall myNetCall){

        //1構造RequestBody
        RequestBody body = setRequestBody(bodyParams);
        //2構造Request
        Request.Builder requestBuilder = new Request.Builder();
        if(Util.Token != null) {
            requestBuilder.addHeader("Authentication", Util.Token);
        }
        Request request = requestBuilder.post(body).url(url).build();
        Log.e("NetUtil", String.valueOf(request.headers()));
        //3將Request封裝為Call
        Call call = mOkhttpClient.newCall(request);
        //4執行Call
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                myNetCall.faild(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                myNetCall.success(call, response);
            }
        });
    }
    /**
     * POST的請求參數，構造RequestBody
     * @param BodyParams
     * @return
     */
    private RequestBody setRequestBody(Map<String,String> BodyParams){
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject jsonObject = new JSONObject(BodyParams);
        RequestBody body = RequestBody.create(JSON,jsonObject.toString());
        return body;
    }

}
