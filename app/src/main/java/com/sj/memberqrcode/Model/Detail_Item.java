package com.sj.memberqrcode.Model;

public class Detail_Item {
    private String DateTime;
    private String Money;
    private String Note;

    public Detail_Item(String dateTime, String money, String note) {
        DateTime = dateTime;
        Money = money;
        Note = note;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getMoney() {
        return Money;
    }

    public void setMoney(String money) {
        Money = money;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }
}
