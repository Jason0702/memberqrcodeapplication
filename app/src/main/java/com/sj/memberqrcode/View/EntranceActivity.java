package com.sj.memberqrcode.View;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.sj.memberqrcode.Control.PagerAdapter;
import com.sj.memberqrcode.R;

import java.util.ArrayList;
import java.util.List;

//TabLayout
public class EntranceActivity extends AppCompatActivity {
    private List<Fragment> fragments = new ArrayList<>();
    private List<String> titles = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrance);
        initData();
        initView();
    }
    void initView(){
        TabLayout mTableLayout = findViewById(R.id.tabLayout);
        ViewPager mViewPager = findViewById(R.id.vp_view);
        mViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),EntranceActivity.this,fragments,titles));
        mTableLayout.setupWithViewPager(mViewPager);//此方法就是讓tablayout和ViewPager連動
    }
    void initData(){

        fragments.add(QRCodrActivity.newInstance(getResources().getString(R.string.QRCodeActivity)));
        fragments.add(InquireActivity.newInstance(getResources().getString(R.string.InquireActivity)));
        fragments.add(MemberDataActivity.newInstance(getResources().getString(R.string.MemberDataActivity)));

        titles.add(getResources().getString(R.string.QRCodeActivity));
        titles.add(getResources().getString(R.string.InquireActivity));
        titles.add(getResources().getString(R.string.MemberDataActivity));

    }
}
