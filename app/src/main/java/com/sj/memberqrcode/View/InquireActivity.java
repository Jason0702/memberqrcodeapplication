package com.sj.memberqrcode.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.sj.memberqrcode.Control.BaseFragment;
import com.sj.memberqrcode.Control.InquireAdapter;
import com.sj.memberqrcode.Control.NetUtils;
import com.sj.memberqrcode.Control.Util;
import com.sj.memberqrcode.Model.Detail_Item;
import com.sj.memberqrcode.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class InquireActivity extends BaseFragment {

    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    NetUtils netUtils;

    RecyclerView recyclerView;
    List<Detail_Item> items = new ArrayList<>();
    InquireAdapter adapter;

    Button StoreButton, ShoppingButton;
    ImageView StartCalendar_View, OverCalendar_View;
    EditText StartDate,EndDate;

    int year,month,day;

    String UpStartTime,UpEndTime;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.activity_inquire,container,false);

            netUtils = NetUtils.getInstance();


            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();

            //region 取得系統日期
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day  = calendar.get(Calendar.DAY_OF_MONTH);
            DayTime(year,month,day);
            OverDayTime(year,month,day);
            StartDate.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",day));
            EndDate.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",day));
            GetDateShopping();

        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }
    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        StoreButton = mView.findViewById(R.id.StoreButton);
        ShoppingButton = mView.findViewById(R.id.ShoppingButton);
        StoreButton.setOnClickListener(Click);
        ShoppingButton.setOnClickListener(Click);

        StartCalendar_View = mView.findViewById(R.id.StartCalendar_View);
        OverCalendar_View = mView.findViewById(R.id.OverCalendar_View);
        StartCalendar_View.setOnClickListener(Click);
        OverCalendar_View.setOnClickListener(Click);

        StartDate = mView.findViewById(R.id.StratDate);
        EndDate = mView.findViewById(R.id.EndDate);
        StartDate.setEnabled(false);
        EndDate.setEnabled(false);

        recyclerView = mView.findViewById(R.id.InquireListView);

        adapter = new InquireAdapter(items);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }
    public static InquireActivity newInstance(String param){
        InquireActivity inquireActivity = new InquireActivity();
        Bundle args = new Bundle();
        args.putString("agrs1", param);
        inquireActivity.setArguments(args);
        return inquireActivity;
    }
    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.StoreButton:
                items.clear();
                GetDateStored();
                break;
            case R.id.ShoppingButton:
                items.clear();
                GetDateShopping();
                break;
            case R.id.StartCalendar_View:
                OpenPopCalender(v);
                break;
            case R.id.OverCalendar_View:
                OpenOverPopCalender(v);
                break;
        }
    };

    private void OpenPopCalender(View v){
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_popcalendar,null,false);
        CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAsDropDown(v,0,0);

        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            StartDate.setText("");
            StartDate.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            DayTime(year,month,dayOfMonth);

            popupWindow.dismiss();
        });
    }
    private void DayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        UpStartTime = sdf.format(new Date(eventOccursOn));
        Log.e("PUTTime","Date: " + UpStartTime);
    }

    private void OpenOverPopCalender(View v){
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_popcalendar,null,false);
        CalendarView calendarView = view.findViewById(R.id.calendar);

        final PopupWindow popupWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,true);

        popupWindow.setTouchable(true);
        popupWindow.setTouchInterceptor((v1, event) -> false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));

        popupWindow.showAsDropDown(v,0,0);

        calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
            EndDate.setText("");
            EndDate.setText(String.format("%04d",year) + "/" + String.format("%02d",(month+1)) + "/" + String.format("%02d",dayOfMonth));
            OverDayTime(year,month,dayOfMonth);

            popupWindow.dismiss();
        });
    }
    private void OverDayTime(int year,int month,int day){
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        long eventOccursOn = c.getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        UpEndTime= sdf.format(new Date(eventOccursOn));
        Log.e("PUTTime","Date: " + UpEndTime);
    }


    void GetShopping(){
        if(items.size()==0) {
            items.clear();
        }
        netUtils.getDataAsynFromNet(netUtils.GetShoppingRecord + Util.UserID , new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e("Member", String.valueOf(response.header("Content-Type")));

                //Tokene如過期直接換
                /*try {
                    if (!response.header("Authorization").isEmpty()) {
                        Util.Token = response.header("Authorization");
                        Log.e("MemberToken", Util.Token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e("Member", code);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i = 0; i<jsonArray.length();i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Log.e("Shopping",jsonObject1.toString());
                        String Id = jsonObject1.getString("id");
                        String Store_number = jsonObject1.getString("store_number");
                        String Serial_number = jsonObject1.getString("serial_number");
                        String Vip_number = jsonObject1.getString("vip_number");
                        String Vip_name = jsonObject1.getString("vip_name");
                        String Deposit_pay_money = jsonObject1.getString( "deposit_pay_money");
                        String Total_price = jsonObject1.getString("total_price");
                        String Is_valid = jsonObject1.getString( "is_valid");
                        String Remark = jsonObject1.getString( "remark");
                        String Created_at= jsonObject1.getString( "created_at");
                        String Updated_at = jsonObject1.getString( "updated_at");
                        Detail_Item detail_item = new Detail_Item(Updated_at,Total_price,Remark);
                        items.add(detail_item);
                    }
                    getActivity().runOnUiThread(()->{
                        adapter.notifyDataSetChanged();
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }
    void GetStored(){
        if(items.size()==0) {
            items.clear();
        }
        netUtils.getDataAsynFromNet(netUtils.GetStoredRecord + Util.UserID , new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e("Member", String.valueOf(response.header("Content-Type")));

                //Tokene如過期直接換
               /* try {
                    if (!response.header("Authorization").isEmpty()) {
                        Util.Token = response.header("Authorization");
                        Log.e("MemberToken", Util.Token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e("Member", code);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i = 0; i<jsonArray.length();i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Log.e("Stored",jsonObject1.toString());
                        String Id = jsonObject1.getString("id");
                        String Vip_id = jsonObject1.getString("vip_id");
                        String Store_number = jsonObject1.getString("store_number");
                        String Serial_number = jsonObject1.getString("serial_number");
                        String Vip_number = jsonObject1.getString("vip_number");
                        String Vip_name = jsonObject1.getString("vip_name");
                        String Cash_money = jsonObject1.getString("cash_money");
                        String Deposit_money = jsonObject1.getString( "deposit_money");
                        String Work_place = jsonObject1.getString( "work_place");
                        String Pos_class = jsonObject1.getString( "pos_class");
                        String Is_valid = jsonObject1.getString( "is_valid");
                        String Remark = jsonObject1.getString( "remark");
                        String Created_at= jsonObject1.getString( "created_at");
                        String Updated_at = jsonObject1.getString( "updated_at");
                        Detail_Item detail_item;
                        if(Remark.equals("null")) {
                            detail_item = new Detail_Item(Updated_at, Deposit_money, "無");
                        }else{
                            detail_item = new Detail_Item(Updated_at, Deposit_money, Remark);
                        }
                        items.add(detail_item);
                    }
                    getActivity().runOnUiThread(()->{
                        adapter.notifyDataSetChanged();
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }

    void GetDateShopping(){
        if(items.size()==0) {
            items.clear();
        }
        String URL = "http://dev.twgo.asia/member-qrcode/api/v1/vip-self/start/"+ UpStartTime +"/end/"+ UpEndTime+"/vip-shopping-record";
        netUtils.getDataAsynFromNet(URL , new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e("Member", String.valueOf(response.header("Content-Type")));

                //Tokene如過期直接換
                /*try {
                    if (!response.header("Authorization").isEmpty()) {
                        Util.Token = response.header("Authorization");
                        Log.e("MemberToken", Util.Token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e("Member", code);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i = 0; i<jsonArray.length();i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Log.e("Shopping",jsonObject1.toString());
                        String Id = jsonObject1.getString("id");
                        String Store_number = jsonObject1.getString("store_number");
                        String Serial_number = jsonObject1.getString("serial_number");
                        String Vip_number = jsonObject1.getString("vip_number");
                        String Vip_name = jsonObject1.getString("vip_name");
                        String Deposit_pay_money = jsonObject1.getString( "deposit_pay_money");
                        String Total_price = jsonObject1.getString("total_price");
                        String Is_valid = jsonObject1.getString( "is_valid");
                        String Remark = jsonObject1.getString( "remark");
                        String Created_at= jsonObject1.getString( "created_at");
                        String Updated_at = jsonObject1.getString( "updated_at");
                        Detail_Item detail_item = new Detail_Item(Updated_at,Total_price,Remark);
                        items.add(detail_item);
                    }
                    getActivity().runOnUiThread(()->{
                        adapter.notifyDataSetChanged();
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }
    void GetDateStored(){
        if(items.size()==0) {
            items.clear();
        }
        String URL = "http://dev.twgo.asia/member-qrcode/api/v1/vip-self/start/"+UpStartTime+"/end/"+UpEndTime+"/vip-stored-record";
        netUtils.getDataAsynFromNet(URL , new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e("Member", String.valueOf(response.header("Content-Type")));

                //Tokene如過期直接換
               /* try {
                    if (!response.header("Authorization").isEmpty()) {
                        Util.Token = response.header("Authorization");
                        Log.e("MemberToken", Util.Token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e("Member", code);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i = 0; i<jsonArray.length();i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Log.e("Stored",jsonObject1.toString());
                        String Id = jsonObject1.getString("id");
                        String Vip_id = jsonObject1.getString("vip_id");
                        String Store_number = jsonObject1.getString("store_number");
                        String Serial_number = jsonObject1.getString("serial_number");
                        String Vip_number = jsonObject1.getString("vip_number");
                        String Vip_name = jsonObject1.getString("vip_name");
                        String Cash_money = jsonObject1.getString("cash_money");
                        String Deposit_money = jsonObject1.getString( "deposit_money");
                        String Work_place = jsonObject1.getString( "work_place");
                        String Pos_class = jsonObject1.getString( "pos_class");
                        String Is_valid = jsonObject1.getString( "is_valid");
                        String Remark = jsonObject1.getString( "remark");
                        String Created_at= jsonObject1.getString( "created_at");
                        String Updated_at = jsonObject1.getString( "updated_at");
                        Detail_Item detail_item;
                        if(Remark.equals("null")) {
                            detail_item = new Detail_Item(Updated_at, Deposit_money, "無");
                        }else{
                            detail_item = new Detail_Item(Updated_at, Deposit_money, Remark);
                        }
                        items.add(detail_item);
                    }
                    getActivity().runOnUiThread(()->{
                        adapter.notifyDataSetChanged();
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }
}