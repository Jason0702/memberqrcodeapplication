package com.sj.memberqrcode.View;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sj.memberqrcode.Control.NetUtils;
import com.sj.memberqrcode.Control.Util;
import com.sj.memberqrcode.R;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";
    TextView RegisteredTextView;
    Button Sign_inButton;
    NetUtils netUtils;
    EditText AccountSecretEdit, PassWordEdit;
    Intent intent = new Intent();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    void initView(){
        RegisteredTextView = findViewById(R.id.Registered_TextView);
        RegisteredTextView.setOnClickListener(Click);
        Sign_inButton = findViewById(R.id.Sign_in_Button);
        Sign_inButton.setOnClickListener(Click);

        AccountSecretEdit = findViewById(R.id.Account_secret_editText);
        PassWordEdit = findViewById(R.id.PassWord_editText);
    }

    View.OnClickListener Click = v -> {

        switch (v.getId()){
            case R.id.Sign_in_Button:
                String Account = AccountSecretEdit.getText().toString();
                String Password = PassWordEdit.getText().toString();
                if(!Account.isEmpty() && !Password.isEmpty()){
                    POSTUserLogin(Account,Password);
                }else{
                    Toast.makeText(getApplicationContext(),"帳號密碼請勿為空",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.Registered_TextView:
                intent.setClass(this,RegisteredActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    };

    void POSTUserLogin(String Account, String Password){
        Map<String,String> User = new HashMap<>();
        User.put("account",Account);
        User.put("password", Password);

        netUtils = NetUtils.getInstance();
        netUtils.postDataAsynToNet(netUtils.LoginURL, User, new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e(TAG, String.valueOf(response.headers()));
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    Log.e(TAG,jsonObject1.toString());
                    //取得Token
                    Util.Token = jsonObject1.getString("token");
                    //取得ID
                    Util.UserID = jsonObject1.getString("id");

                    Log.e(TAG, code);


                    intent.setClass(MainActivity.this,EntranceActivity.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void faild(Call call, IOException e) {
                ErrorAlertDialog();
                e.printStackTrace();
            }
        });
    }

    void ErrorAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("登入失敗");
        builder.setTitle("提示!");
        builder.setPositiveButton("我知道了!", (dialog, which) -> dialog.dismiss());
    }
}
