package com.sj.memberqrcode.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.Toast;

import com.sj.memberqrcode.Control.BaseFragment;
import com.sj.memberqrcode.Control.NetUtils;
import com.sj.memberqrcode.Control.Util;
import com.sj.memberqrcode.R;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Response;

public class MemberDataActivity extends BaseFragment {
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;
    NetUtils netUtils;
    EditText MemberNameEdit,MemberPasswordEdit, MemberPasswordCheckEdit;
    Button ModifyButton;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.activity_member_data,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();

        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }
    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        MemberNameEdit = mView.findViewById(R.id.MemberNameEdit);
        MemberPasswordEdit = mView.findViewById(R.id.MemberPasswordEdit);
        MemberPasswordCheckEdit = mView.findViewById(R.id.MemberPasswordCheck);
        try {
            if (!Util.MemberName.isEmpty()) {
                MemberNameEdit.setText(Util.MemberName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        ModifyButton = mView.findViewById(R.id.ModifyButton);
        ModifyButton.setOnClickListener(Click);
    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }
    public static MemberDataActivity newInstance(String param){
        MemberDataActivity memberDataActivity = new MemberDataActivity();
        Bundle args = new Bundle();
        args.putString("agrs1", param);
        memberDataActivity.setArguments(args);
        return memberDataActivity;
    }
    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.ModifyButton:
                String MemberName = MemberNameEdit.getText().toString();
                String MemberPassword = MemberPasswordEdit.getText().toString();
                String MemberPasswordCheck = MemberPasswordCheckEdit.getText().toString();
                if(MemberPassword.equals(MemberPasswordCheck)){
                    PUTMember(MemberName,MemberPassword);
                }else{
                    Toast.makeText(getContext(),"請確認密碼是否輸入正確",Toast.LENGTH_LONG);
                }
                break;
        }
    };




    void PUTMember(String Name,String PassWord){

        Log.e("PUT",Name + PassWord);
        Map<String,String> User = new HashMap<>();
        User.put("name",Name);
        User.put("password", PassWord);
        netUtils = NetUtils.getInstance();
        netUtils.putDataAsynToNet(netUtils.GetMemberURL + Util.UserID, User, new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Tokene如過期直接換
               /* try {
                    if (!response.header("Authorization").isEmpty()){
                        Util.Token = response.header("Authorization");
                        Log.e("MemberToken",Util.Token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e("Member", code);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    Log.e("Member", jsonObject.toString());
                    //取得名稱
                    if(code.equals("201")){
                        ConfirmAlertDialog();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }
    void ConfirmAlertDialog(){
        getActivity().runOnUiThread(()->{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("修改成功");
            builder.setTitle("提示!");
            builder.setPositiveButton("我知道了!", (dialog, which) -> {
                MemberPasswordEdit.setText("");
                MemberPasswordCheckEdit.setText("");
            });
            builder.show();
        });
    }
}