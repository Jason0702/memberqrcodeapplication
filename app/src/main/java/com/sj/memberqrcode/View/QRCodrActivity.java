package com.sj.memberqrcode.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.sj.memberqrcode.Control.BaseFragment;
import com.sj.memberqrcode.Control.NetUtils;
import com.sj.memberqrcode.Control.Util;
import com.sj.memberqrcode.R;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

public class QRCodrActivity extends BaseFragment {
    //標誌位，標誌已經初始化完成
    private boolean isPrepared;
    //是否已被加載過一次，第二次就不再去請求數據了
    private boolean mHasLoadedOnce;

    NetUtils netUtils;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null){
            //需要inflate一個次局文件，填充Fragment
            mView = inflater.inflate(R.layout.activity_qrcodr,container,false);
            initView();
            isPrepared = true;
            //實現懶加載
            lazyLoad();
            netUtils = NetUtils.getInstance();

        }
        //緩存的mView需要判斷是否已經被加過parent,如果有parent需要從parent刪除,要不然會發生這個mView已經有的parent的錯誤
        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null){
            parent.removeView(mView);
        }
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        GetMember();
    }

    /**
     * 初始化物件
     */
    private void initView(){
        Bundle bundle = getArguments();
        String args = bundle.getString("agrs1");
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(Util.UserID, BarcodeFormat.QR_CODE, 300, 300);
            ImageView QRCodeImage = mView.findViewById(R.id.QRCodeimageView);
            QRCodeImage.setImageBitmap(bitmap);
        } catch(Exception e) {

        }


    }

    @Override
    public void lazyLoad() {
        if (!isPrepared || !isVisible || mHasLoadedOnce){
            return;
        }
        //填充各物件的數據
        mHasLoadedOnce = true;
    }
    public static QRCodrActivity newInstance(String param){
        QRCodrActivity startActivity = new QRCodrActivity();
        Bundle args = new Bundle();
        args.putString("agrs1", param);
        startActivity.setArguments(args);
        return startActivity;
    }
    View.OnClickListener Click = v -> {
        Intent intent = new Intent();
        switch (v.getId()){

        }
        startActivity(intent);
    };

    void GetMember(){
        netUtils.getDataAsynFromNet(netUtils.GetMemberURL + Util.UserID , new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e("Member", String.valueOf(response.header("Content-Type")));

                //Tokene如過期直接換
                /*try {
                    if (!response.header("Authorization").isEmpty()) {
                        Util.Token = response.header("Authorization");
                        Log.e("MemberToken", Util.Token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e("Member", code);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    Log.e("Member", jsonObject.toString());
                    //取得名稱
                    String MemberName = jsonObject1.getString("name");
                    if(code.equals("200")){
                        Util.MemberName = MemberName;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                e.printStackTrace();
            }
        });
    }
}

