package com.sj.memberqrcode.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sj.memberqrcode.Control.NetUtils;
import com.sj.memberqrcode.Control.Util;
import com.sj.memberqrcode.R;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class RegisteredActivity extends AppCompatActivity {

    final String TAG = "RegisteredActivity";
    Button RegisteredButton, CancelButton;
    EditText AccountSecretEdit, PassWordEdit, UserNameEdit, PassWordCheck;
    NetUtils netUtils;
    Intent intent = new Intent();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered);
        initView();
    }
    void initView(){
        RegisteredButton = findViewById(R.id.RegisteredButton);
        RegisteredButton.setOnClickListener(Click);
        CancelButton = findViewById(R.id.CancelButton);
        CancelButton.setOnClickListener(Click);

        AccountSecretEdit = findViewById(R.id.Account_secret_editText);
        PassWordEdit = findViewById(R.id.PassWord_editText);
        UserNameEdit = findViewById(R.id.UserNameEditText);
        PassWordCheck = findViewById(R.id.PassWordCheckEditText);
    }

    View.OnClickListener Click = v -> {
        switch (v.getId()){
            case R.id.CancelButton:
                intent.setClass(this,MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.RegisteredButton:
                String UserName = UserNameEdit.getText().toString();
                String Account = AccountSecretEdit.getText().toString();
                String Password = PassWordEdit.getText().toString();
                String PasswordCheck = PassWordCheck.getText().toString();
                if(Password.equals(PasswordCheck)) {
                    if (!Account.isEmpty() && !Password.isEmpty() && !PasswordCheck.isEmpty() && !UserName.isEmpty()) {
                        POSTRegistered(UserName,Account,Password);
                    }else{
                        if(Account.isEmpty()){
                            Toast.makeText(getApplicationContext(),"手機號碼請勿空白",Toast.LENGTH_LONG).show();
                        }else if(Password.isEmpty()){
                            Toast.makeText(getApplicationContext(),"密碼請勿空白",Toast.LENGTH_LONG).show();
                        }else if(PasswordCheck.isEmpty()){
                            Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
                        }
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"請確認密碼是否正確",Toast.LENGTH_LONG);
                }
                break;
        }
    };
    void POSTRegistered(String Name, String Account, String Password){
        Map<String,String> User = new HashMap<>();
        User.put("name",Name);
        User.put("account",Account);
        User.put("password", Password);

        netUtils = NetUtils.getInstance();
        netUtils.postDataAsynToNet(netUtils.RegisteredURL, User, new NetUtils.MyNetCall() {
            @Override
            public void success(Call call, Response response) throws IOException {
                //Log.e(TAG, String.valueOf(response.headers()));
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String code = jsonObject.getString("code");
                    Log.e(TAG, code);
                    if(code.equals("201")){
                        ConfirmAlertDialog();
                    }else{
                        ErrorAlertDialog();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void faild(Call call, IOException e) {
                ErrorAlertDialog();
                e.printStackTrace();
            }
        });
    }
    void ConfirmAlertDialog(){
        runOnUiThread(()->{
            AlertDialog.Builder builder = new AlertDialog.Builder(RegisteredActivity.this);
            builder.setMessage("註冊成功");
            builder.setTitle("提示!");
            builder.setPositiveButton("我知道了!", (dialog, which) -> {
                intent.setClass(RegisteredActivity.this, MainActivity.class);
                startActivity(intent);
                dialog.dismiss();
                finish();
            });
            builder.show();
        });
    }

    void ErrorAlertDialog(){
        runOnUiThread(()->{
            AlertDialog.Builder builder = new AlertDialog.Builder(RegisteredActivity.this);
            builder.setMessage("註冊失敗");
            builder.setTitle("提示!");
            builder.setPositiveButton("我知道了!", (dialog, which) -> dialog.dismiss());
            builder.show();
        });
    }
}
